<?php
	require_once('db.connect.php');
	class Unite{
		private $id = 0;
        private $libelle = "";
		
		public function getId(){
			return $this->id;
		}
		
		public function getLibelle(){
			return $this->libelle;
		}
		
		public function __construct($_id,$_libelle){
			$this->id = $_id;
			$this->libelle = $_libelle;
		}
		
		public static function createFromRequest($request){
			$_id = $request->idUnite;
			$_libelle = $request->libUnite;
			$instance = new self($_id,$_libelle);
			return $instance;
		}
	}
	
	class DbUnites{
		private $pdo = null;
        private static $table = '"public"."Unite"';
		
		public static function create(){
			$instance = new self(connect_to_db());
			return $instance;
		}
        
		public function __construct($_pdo){
			$this->pdo = $_pdo;
		}
		
		public function getUnite($id){
			$request = $this->pdo->prepare("SELECT * FROM ".self::$table." WHERE ".self::$table.".\"idUnite\" = ?");
			if ($request->execute(array($id)) != null){
				$fetchedRequest = $request->fetchAll();
				if (!empty($fetchedRequest)){
					return Unite::createFromRequest($fetchedRequest[0]);
				} else {
					throw new Exception('Unite does not exist.');
				}
			} else {
				throw new Exception('Request failed. Contact an administrator to fix this issue.');
			}
		}
        
        public function getUnites(){
            $request = $this->pdo->prepare("SELECT * FROM ".self::$table." ORDER BY ".self::$table.".\"libUnite\"");
			if ($request->execute()){
				$fetchedRequest = $request->fetchAll();
				if (!empty($fetchedRequest)){
					$unites = array();
					foreach($fetchedRequest as $unite){
						$unites[$unite->idUnite] = Unite::createFromRequest($unite);
					}
					return $unites;
				} else {
					throw new Exception('Unite does not exist.');
				}
			} else {
				throw new Exception('Request failed. Contact an administrator to fix this issue.');
			}
        }
        
        public function insertUnite($libelle){
            $request = $this->pdo->prepare("INSERT INTO ".self::$table." (\"libUnite\") VALUES (?)");
            if (!($request->execute(array($libelle)))){
				throw new Exception('Request failed. Contact an administrator to fix this issue.');
			}
        }
	}
?>