<?php
	require_once('../model/db.connect.php');
	class User{
		private $mail = "";
		private $password = "";
        private $nom = "";
        private $prenom = "";
		private $admin = false;
        private $token = null;
        private $address = "";
        private $phoneNumber = "";
		
		public function getMail(){
			return $this->mail;
		}
		
		public function getPassword(){
			return $this->password;
		}
        
        public function getNom(){
			return $this->nom;
		}
        
        public function getPrenom(){
			return $this->prenom;
		}
		
		public function isAdmin(){
			return $this->admin;
		}
        
        public function getResetToken(){
            return $this->token;
        }
        
        public function getAddress(){
            return $this->address;
        }
        
        public function getPhoneNumber(){
            return $this->phoneNumber;
        }
		
		public function __construct($_mail,$_password,$_nom,$_prenom,$_admin,$_token,$_address,$_phoneNumber){
			$this->mail = $_mail;
			$this->password = $_password;
			$this->nom = $_nom;
			$this->prenom = $_prenom;
			$this->admin = $_admin;
            $this->token = $_token;
            $this->address = $_address;
            $this->phoneNumber = $_phoneNumber;
		}
		
		public static function createFromRequest($request){
			$_mail = $request->mail;
			$_password = $request->password;
			$_nom = $request->nom;
			$_prenom = $request->prenom;
			$_admin = $request->isAdmin;
            $_token = $request->resetToken;
            $_address = $request->adresse;
            $_phoneNumber = $request->numTel;
			$instance = new self($_mail,$_password,$_nom,$_prenom,$_admin,$_token,$_address,$_phoneNumber);
			return $instance;
		}
	}
	
	class DbUsers{
		private $pdo = null;
        private static $table = '"public"."Utilisateur"';
		
		public static function create(){
			$instance = new self(connect_to_db());
			return $instance;
		}
		
		public function __construct($_pdo){
			$this->pdo = $_pdo;
		}
		
		public function getUser($mailUser){
			$request = $this->pdo->prepare("SELECT * FROM ".self::$table." WHERE \"mail\" = ?");
			if ($request->execute(array($mailUser)) != null){
				$fetchedRequest = $request->fetchAll();
				if (!empty($fetchedRequest)){
					return User::createFromRequest($fetchedRequest[0]);
				} else {
					throw new Exception('User does not exist.');
				}
			} else {
				throw new Exception('Request failed. Contact an administrator to fix this issue.');
			}
		}
        
        public function insertUser($mail,$password,$nom,$prenom,$address,$phoneNumber){
            $request = $this->pdo->prepare("INSERT INTO ".self::$table." (\"mail\",\"password\",\"nom\",\"prenom\",\"adresse\",\"numTel\") VALUES (?,?,?,?,?,?)");
            if (!($request->execute(array($mail,$password,$nom,$prenom,$address,$phoneNumber)))){
				throw new Exception('Request failed. Contact an administrator to fix this issue.');
			}
        }
        
        public function getUserByToken($token){
            $request = $this->pdo->prepare("SELECT * FROM ".self::$table." WHERE \"resetToken\" = ?");
			if ($request->execute(array($token)) != null){
				$fetchedRequest = $request->fetchAll();
				if (!empty($fetchedRequest)){
					return User::createFromRequest($fetchedRequest[0]);
				} else {
					throw new Exception('User does not exist.');
				}
			} else {
				throw new Exception('Request failed. Contact an administrator to fix this issue.');
			}
        }
        
        public function setUserAddress($mail,$address){
            $request = $this->pdo->prepare("UPDATE ".self::$table." SET \"adresse\"=? WHERE \"mail\"=?");
            if (!($request->execute(array(isset($address)?$address:NULL,$mail)))){
				throw new Exception('Request failed. Contact an administrator to fix this issue.');
			}
        }
        
        public function setUserPhoneNumber($mail,$phoneNumber){
            $request = $this->pdo->prepare("UPDATE ".self::$table." SET \"numTel\"=? WHERE \"mail\"=?");
            if (!($request->execute(array(isset($phoneNumber)?$phoneNumber:NULL,$mail)))){
				throw new Exception('Request failed. Contact an administrator to fix this issue.');
			}
        }
        
        public function setUserToken($mail,$token){
            $request = $this->pdo->prepare("UPDATE ".self::$table." SET \"resetToken\"=? WHERE \"mail\"=?");
            if (!($request->execute(array(isset($token)?$token:NULL,$mail)))){
				throw new Exception('Request failed. Contact an administrator to fix this issue.');
			}
        }
        
        public function setUserPassword($mail,$password){
            $request = $this->pdo->prepare("UPDATE ".self::$table." SET \"password\"=? WHERE \"mail\"=?");
            if (!($request->execute(array($password,$mail)))){
				throw new Exception('Request failed. Contact an administrator to fix this issue.');
			}
        }
	}
?>