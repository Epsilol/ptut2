<?php
	require_once('db.connect.php');
	class Accueil{
        private $textAccueil = "";
		
		public function getTextAccueil(){
			return $this->textAccueil;
		}
		
		
		public function __construct($_textAccueil){
            $this->textAccueil = $_textAccueil;
		}
		
		public static function createFromRequest($request){
			$_textAccueil = $request->textAccueil;
			$instance = new self($_textAccueil);
			return $instance;
		}
	}
	
	class DbAccueil{
		private $pdo = null;
        private static $table = '"public"."Accueil"';
		
		public static function create(){
			$instance = new self(connect_to_db());
			return $instance;
		}
		
		public function __construct($_pdo){
			$this->pdo = $_pdo;
		}
		
		public function getAccueil(){
			$request = $this->pdo->prepare("SELECT * FROM ".self::$table);
			if ($request->execute() != null){
				$fetchedRequest = $request->fetchAll();
				if (!empty($fetchedRequest)){
					return Accueil::createFromRequest($fetchedRequest[0]);
				} else {
					throw new Exception('Accueil does not exist.');
				}
			} else {
				throw new Exception('Request failed. Contact an administrator to fix this issue.');
			}
		}
		
		public function updateAccueil($textAccueil){
			$request = $this->pdo->prepare("UPDATE ".self::$table." SET \"textAccueil\"=? ");
			if (!($request->execute(array($textAccueil)))){
				throw new Exception('Request failed. Contact an administrator to fix this issue.');
			}
		}
        
	}
?>