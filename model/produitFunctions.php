<?php
    require_once("DbProduits.php");
    function getProduits(){
        $dbProduits = DbProduits::create();
        $produits = $dbProduits->getProduits();
        return $produits;
    }
    
    function getProduitById($id){
        return getProduits()[$id];
    }
    
    function getProduitsForCategorie($idCategorie){
        $produits = getProduits();
        $produitsTriee = array();
        foreach($produits as $produit){
            if($produit->getCategorie()->getId() == $idCategorie){
                $produitsTriee[] = $produit;
            }
        }
        return $produitsTriee;
    }
    
    function getActivatedProduits($produits){
        $activatedProduits = array();
        foreach($produits as $produit) {
            if($produit->isActif()){
                $activatedProduits[] = $produit;
            }
        }
        return $activatedProduits;
    }
    
    function addProduit($nom,$photo,$description,$prix,$idUnite,$idCategorie){
        $dbProduits = DbProduits::create();
        $dbProduits->insertProduit($nom,$photo,$description,$prix,$idUnite,$idCategorie);
    }
    
    function updateProduit($id,$nom,$photo,$description,$prix,$idUnite,$idCategorie){
        $dbProduits = DbProduits::create();
        $dbProduits->updateProduit($id,$nom,$photo,$description,$prix,$idUnite,$idCategorie);
    }
    
    function updateProduitNoPhoto($id,$nom,$description,$prix,$idUnite,$idCategorie){
        $dbProduits = DbProduits::create();
        $dbProduits->updateProduitNoPhoto($id,$nom,$description,$prix,$idUnite,$idCategorie);
    }
    
    function getProduitByNom($nom){
        foreach(getProduits() as $produit){
            if ($produit->getNom()==$nom){
                return $produit;
            }
        }
        return null;
    }
    
    function setProduitPhoto($id,$photo){
        $dbProduits = DbProduits::create();
        $dbProduits->updateProduitPhoto($id,$photo);
    }
    
    function setProduitState($id,$state){
        DbProduits::create()->setProduitState($id,$state);
    }
    
    function deleteProduit($id){
        DbProduits::create()->deleteProduit($id);
    }
    
    function setAllProduitsState($state){
        DbProduits::create()->setAllProduitsState($state);
    }
?>