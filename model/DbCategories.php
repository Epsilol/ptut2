<?php
	require_once('db.connect.php');
	class Categorie{
		private $id = 0;
		private $nom = "";
		private $categorieMaitre = null;
		private $actif = true;
		
		public function getId(){
			return $this->id;
		}
		
		public function getNom(){
			return $this->nom;
		}
		
		public function getCategorieMaitre(){
			return $this->categorieMaitre;
		}
        
        public function isActif(){
			return $this->actif;
		}
		
		public function __construct($_id,$_nom,$_categorieMaitre,$_actif){
			$this->id = $_id;
			$this->nom = $_nom;
			$this->categorieMaitre = $_categorieMaitre;
			$this->actif = $_actif;
		}
		
		public static function createFromRequest($request){
			$_id = $request->idCategorie;
			$_nom = $request->nomCategorie;
			$_categorieMaitre = ($request->categorieMaitre != null ? DbCategories::create()->getCategorie($request->categorieMaitre) : null);
			$_actif = $request->isActif;
			$instance = new self($_id,$_nom,$_categorieMaitre,$_actif);
			return $instance;
		}
	}
	
	class DbCategories{
		private $pdo = null;
        private static $table = '"public"."Categorie"';
		
		public static function create(){
			$instance = new self(connect_to_db());
			return $instance;
		}
		
		public function __construct($_pdo){
			$this->pdo = $_pdo;
		}
		
		public function getCategories(){
			$request = $this->pdo->prepare("SELECT * FROM ".self::$table." ORDER BY ".self::$table.".\"nomCategorie\"");
			if ($request->execute()){
				$fetchedRequest = $request->fetchAll();
				if (!empty($fetchedRequest)){
					$categories = array();
					foreach($fetchedRequest as $categorie){
						$categories[$categorie->idCategorie] = Categorie::createFromRequest($categorie);
					}
					return $categories;
				} else {
					throw new Exception('Categorie does not exist.');
				}
			} else {
				throw new Exception('Request failed. Contact an administrator to fix this issue.');
			}
		}
        
        public function getCategorie($id){
            $request = $this->pdo->prepare("SELECT * FROM ".self::$table." WHERE ".self::$table.".\"idCategorie\" = ?");
			if ($request->execute(array($id)) != null){
				$fetchedRequest = $request->fetchAll();
				if (!empty($fetchedRequest)){
					return Categorie::createFromRequest($fetchedRequest[0]);
				} else {
					throw new Exception('Categorie does not exist.');
				}
			} else {
				throw new Exception('Request failed. Contact an administrator to fix this issue.');
			}
        }
		
		public function insertCategorie($nom,$categorieMaitre=null){
            $request = $this->pdo->prepare("INSERT INTO ".self::$table." (\"nomCategorie\",\"categorieMaitre\") VALUES (?,?)");
			if (!($request->execute(array($nom,$categorieMaitre)))){
				throw new Exception('Request failed. Contact an administrator to fix this issue.');
			}
		}
		
		public function deleteCategorie($id){
			$request = $this->pdo->prepare("DELETE FROM ".self::$table." WHERE ".self::$table.".\"idCategorie\"=?");
			if (!($request->execute(array($id)))){
				throw new Exception('Request failed. Contact an administrator to fix this issue.');
			}
		}
		
		public function updateCategorie($id,$nomCategorie){
			$request = $this->pdo->prepare("UPDATE categorie SET nomCategorie=? WHERE id=?");
			if (!($request->execute(array($nomCategorie,$id)))){
				throw new Exception('Request failed. Contact an administrator to fix this issue.');
			}
		}
        
        public function setCategorieState($id,$isActif){
            $request = $this->pdo->prepare("UPDATE ".self::$table." SET \"isActif\"=? WHERE \"idCategorie\"=?");
			if (!($request->execute(array($isActif,$id)))){
				throw new Exception('Request failed. Contact an administrator to fix this issue.');
			}
        }
        
        public function setAllCategoriesState($isActif){
            echo " >> ",$isActif, " >> ";
            $request = $this->pdo->prepare("UPDATE ".self::$table." SET \"isActif\"=?");
			if (!($request->execute(array($isActif)))){
				throw new Exception('Request failed. Contact an administrator to fix this issue.');
			}
        }
	}
?>