<?php
    require_once('DbCategories.php');
    function getCategories(){
        $dbCategories = DbCategories::create();
        $categories = $dbCategories->getCategories();
        return $categories;
    }
    
    function getParentCategories(){
        $categories = getCategories();
        $parentCategories = array();
        foreach($categories as $categorie) {
            if($categorie->getCategorieMaitre() == null){
                $parentCategories[] = $categorie;
            }
        }
        return $parentCategories;
    }
    
    function getChildCategoriesOf($parentId){
        $categories = getCategories();
        $childCategories = array();
        foreach($categories as $categorie) {
            if($categorie->getCategorieMaitre() != null && $categorie->getCategorieMaitre()->getId() == $parentId){
                $childCategories[] = $categorie;
            }
        }
        return $childCategories;
    }
    
    function getActivatedCategories($categories){
        $activatedCategories = array();
        foreach($categories as $categorie) {
            if($categorie->isActif()){
                $activatedCategories[] = $categorie;
            }
        }
        return $activatedCategories;
    }
    
    function addCategorie($name,$categorieMaitreId=null){
        DbCategories::create()->insertCategorie($name,$categorieMaitreId);
    }
    
    function deleteCategorie($id){
        DbCategories::create()->deleteCategorie($id);
    }
    
    function setCategorieState($id,$state){
        DbCategories::create()->setCategorieState($id,$state);
    }
    
    function setAllCategoriesState($state){
        DbCategories::create()->setAllCategoriesState($state);
    }
?>