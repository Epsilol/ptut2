<?php
    class Cart{
        private const COOKIE_ID = 'cart_items_cookie';
        private const SAVED_DURATION = 60*60*24*30;
        
        private static $instance = null;
        static function getInstance(){
            if(self::$instance == null){
                self::$instance = new self();
            }
            return self::$instance;
        }
        
        private function saveToCookies($cartItems){
            $json = json_encode($cartItems, true);
            setcookie(self::COOKIE_ID, $json, time() + (self::SAVED_DURATION), '/');
            $_COOKIE[self::COOKIE_ID]=$json;
        }
        
        function getProducts(){
            $cookie = isset($_COOKIE[self::COOKIE_ID]) ? $_COOKIE[self::COOKIE_ID] : "";
            $cookie = stripslashes($cookie);
            $savedCartItems = json_decode($cookie, true);
            if(!$savedCartItems){
                $savedCartItems=array();
            }
            return $savedCartItems;
        }
        
        function getProduct($id){
            return $this->getProducts()[$id];
        }
        
        function addProduct($id,$quantity){
            $products = $this->getProducts();
            $products[$id] = $quantity;
            $this->saveToCookies($products);
        }
        
        function removeProduct($id){
            $products = $this->getProducts();
            unset($products[$id]);
            $this->saveToCookies($products);
        }
        
        function clean(){
            $this->saveToCookies(array());
        }
    }
?>