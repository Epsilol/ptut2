<?php
    require_once('DbUnites.php');
    function getUnites(){
        $dbUnites = DbUnites::create();
        $unites = $dbUnites->getUnites();
        return $unites;
    }
    
    function addUnite($libelle){
        $dbUnites = DbUnites::create();
        $dbUnites->insertUnite($libelle);
    }
?>