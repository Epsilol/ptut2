<?php
	$pdo = null;
	function connect_to_db()
	{
		try{
            $ini = parse_ini_file('../config/database.ini');
			$pdo = new PDO($ini['db_engine'].":host=".$ini['db_host'].";dbname=".$ini['db_name'], $ini['db_user'],$ini['db_password']);
			$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
			return $pdo;
		}  
		catch (PDOException $e){
			echo $e->getMessage();
		}
	}
?>