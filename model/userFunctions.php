<?php
	require_once('../model/DbUsers.php');
	function tryConnectUser($_mail,$_password){
		$mail = htmlspecialchars($_mail);
		$password = htmlspecialchars($_password);
		
		//try to connect 
		$dbUsers = DbUsers::create(); 
		$user = $dbUsers->getUser($mail);
		if(password_verify($password,$user->getPassword())){
            return $user;
		} else {
			throw new Exception('Password does not correspond with user\'s password.');
		}
	}
    
    function getUserNomAndPrenom($mail){
        $dbUsers = DbUsers::create(); 
		$user = $dbUsers->getUser($mail);
        return array("nom"=>$user->getNom(),"prenom"=>$user->getPrenom());
    }
    
    function getUserAddressAndPhoneNumber($mail){
        $dbUsers = DbUsers::create(); 
		$user = $dbUsers->getUser($mail);
        return array("phoneNumber"=>$user->getPhoneNumber(),"address"=>$user->getAddress());
    }
	
	function disconnectUser(){
        //efface les variables de session
        $_SESSION = array();
        
        //efface le cookie lié à la session
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }
		session_destroy();
	}
    
    function isUserConnected($mail,$nom,$prenom){
        return (isset($mail)&&$mail!=""&&isset($nom)&&$nom!=""&&isset($prenom)&&$prenom!="");
    }
    
    function hasUserRequestedResetPassword($token){
        if(!isset($token)){
            return false;
        }
        try{
            DbUsers::create()->getUserByToken($token);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    
    function resetUserToken($mail){
        try{
            $token = randomStr(12);
            DbUsers::create()->setUserToken($mail,$token);
            return $token;
        } catch (Exception $e) {
            return null;
        }
    }
    
    function addUser($mail,$hashedPassword,$nom,$prenom,$address,$phoneNumber){
        try{
            DbUsers::create()->insertUser($mail,$hashedPassword,$nom,$prenom,$address,$phoneNumber);
        } catch (Exception $e) {
        }
    }
    
    function changeUserAddress($mail,$address){
        try{
            DbUsers::create()->setUserAddress($mail,$address);
        } catch (Exception $e) {
        }
    }
    
    function changeUserPhoneNumber($mail,$phoneNumber){
        try{
            DbUsers::create()->setUserPhoneNumber($mail,$phoneNumber);
        } catch (Exception $e) {
        }
    }
    
    function changeUserPassword($_mail,$_password,$newPassword){
        $dbUsers = DbUsers::create();
        try {
            echo "start ";
            $mail = htmlspecialchars($_mail);
            $password = htmlspecialchars($_password);
            //verify if user exist
            $user = $dbUsers->getUser($mail);
            //verify if password ok
            if(password_verify($password,$user->getPassword())){
                //update password
                $dbUsers->setUserPassword($mail,$newPassword);
            }
        } catch (Exception $e){
            
        }
    }
    
    function changeUserPasswordWithToken($token,$newPassword){
        $dbUsers = DbUsers::create();
        try {
            $user = $dbUsers->getUserByToken($token);

            //update password
            $dbUsers->setUserPassword($user->getMail(),$newPassword);
            $dbUsers->setUserToken($user->getMail(),NULL);

        } catch (Exception $e){
            
        }
    }
    
    function randomStr(
        $length,
        $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    ) {
        $str = '';
        $max = mb_strlen($keyspace, '8bit') - 1;
        if ($max < 1) {
            throw new Exception('$keyspace must be at least two characters long');
        }
        for ($i = 0; $i < $length; ++$i) {
            $str .= $keyspace[random_int(0, $max)];
        }
        return $str;
    }
?>