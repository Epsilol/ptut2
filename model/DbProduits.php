<?php
	require_once('db.connect.php');
    require_once('DbCategories.php');
    require_once('DbUnites.php');
	class Produit{
		private $id = 0;
		private $nom = "";
        private $photo = "";
        private $description = "";
        private $prix = 0;
        private $actif = true;
		private $unite = null;
        private $categorie = null;
		
		public function getId(){
			return $this->id;
		}
		
		public function getNom(){
			return $this->nom;
		}
		
		public function getPhoto(){
			return $this->photo;
		}
        
        public function getDescription(){
			return $this->description;
		}
        
        public function getPrix(){
            return $this->prix;
        }
        
        public function isActif(){
			return $this->actif;
		}
        
        public function getUnite(){
            return $this->unite;
        }
        
        public function getCategorie(){
            return $this->categorie;
        }
		
		public function __construct($_id,$_nom,$_photo,$_description,$_prix,$_actif,$_unite,$_categorie){
			$this->id = $_id;
			$this->nom = $_nom;
            $this->photo = $_photo;
            $this->description = $_description;
            $this->prix = $_prix;
			$this->actif = $_actif;
            $this->unite = $_unite;
            $this->categorie = $_categorie;
		}
		
		public static function createFromRequest($request){
			$_id = $request->idProduit;
            $_nom = $request->nomProduit;
            $_photo = $request->nomPhoto;
            $_description = $request->descProduit;
            $_prix = $request->prixProduit;
			$_actif = $request->isActif;
            $_unite = DbUnites::create()->getUnite($request->unite);
            $_categorie = DbCategories::create()->getCategorie($request->categorie);
			$instance = new self($_id,$_nom,$_photo,$_description,$_prix,$_actif,$_unite,$_categorie);
			return $instance;
		}
	}
	
	class DbProduits{
		private $pdo = null;
        private static $table = '"public"."Produit"';
		
		public static function create(){
			$instance = new self(connect_to_db());
			return $instance;
		}
		
		public function __construct($_pdo){
			$this->pdo = $_pdo;
		}
		
		public function getProduits(){
			$request = $this->pdo->prepare("SELECT * FROM ".self::$table." ORDER BY ".self::$table.".\"nomProduit\"");
			if ($request->execute()){
				$fetchedRequest = $request->fetchAll();
				if (!empty($fetchedRequest)){
					$produits = array();
					foreach($fetchedRequest as $produit){
						$produits[$produit->idProduit] = Produit::createFromRequest($produit);
					}
					return $produits;
				} else {
					throw new Exception('Produit does not exist.');
				}
			} else {
				throw new Exception('Request failed. Contact an administrator to fix this issue.');
			}
		}
		
		public function insertProduit($nom,$photo,$description,$prix,$idUnite,$idCategorie){
			$request = $this->pdo->prepare("INSERT INTO ".self::$table." (\"nomProduit\",\"nomPhoto\",\"descProduit\",\"prixProduit\",\"unite\",\"categorie\") VALUES (?,?,?,?,?,?)");
			if (!($request->execute(array($nom,$photo,$description,$prix,$idUnite,$idCategorie)))){
				throw new Exception('Request failed. Contact an administrator to fix this issue.');
			}
		}
		
		public function deleteProduit($id){
			$request = $this->pdo->prepare("DELETE FROM ".self::$table." WHERE ".self::$table.".\"idProduit\"=?");
			if (!($request->execute(array($id)))){
				throw new Exception('Request failed. Contact an administrator to fix this issue.');
			}
		}
		
		public function updateProduitPhoto($id,$photo){
			$request = $this->pdo->prepare("UPDATE ".self::$table." SET \"nomPhoto\"=? WHERE \"idProduit\"=?");
			if (!($request->execute(array($photo,$id)))){
				throw new Exception('Request failed. Contact an administrator to fix this issue.');
			}
		}
        
        public function updateProduit($id,$nom,$photo,$description,$prix,$idUnite,$idCategorie){
			$request = $this->pdo->prepare("UPDATE ".self::$table." SET \"nomProduit\"=?,\"nomPhoto\"=?,\"descProduit\"=?,\"prixProduit\"=?,\"unite\"=?,\"categorie\"=? WHERE \"idProduit\"=?");
			if (!($request->execute(array($nom,$photo,$description,$prix,$idUnite,$idCategorie,$id)))){
				throw new Exception('Request failed. Contact an administrator to fix this issue.');
			}
		}
        
        public function updateProduitNoPhoto($id,$nom,$description,$prix,$idUnite,$idCategorie){
			$request = $this->pdo->prepare("UPDATE ".self::$table." SET \"nomProduit\"=?,\"descProduit\"=?,\"prixProduit\"=?,\"unite\"=?,\"categorie\"=? WHERE \"idProduit\"=?");
			if (!($request->execute(array($nom,$description,$prix,$idUnite,$idCategorie,$id)))){
				throw new Exception('Request failed. Contact an administrator to fix this issue.');
			}
		}
        
        public function setProduitState($id,$isActif){
            $request = $this->pdo->prepare("UPDATE ".self::$table." SET \"isActif\"=? WHERE \"idProduit\"=?");
			if (!($request->execute(array($isActif,$id)))){
				throw new Exception('Request failed. Contact an administrator to fix this issue.');
			}
        }
        
        public function setAllProduitsState($isActif){
            $request = $this->pdo->prepare("UPDATE ".self::$table." SET \"isActif\"=?");
			if (!($request->execute(array($isActif)))){
				throw new Exception('Request failed. Contact an administrator to fix this issue.');
			}
        }
	}
?>