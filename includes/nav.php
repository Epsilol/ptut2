<?php
require_once("../model/categorieFunctions.php");
?>
<!-- Menu -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src='https://kit.fontawesome.com/a076d05399.js'></script>
<nav class="navbar navbar-expand-xl bg-nav navbar-light fixed-top">
  <!-- Brand -->
  <div class="navbar-brand" href="#"><img src="../img/logo.png" alt="logo" class="logo"/>
   Les Bons Plants du Puech</div>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
     <span class="navbar-toggler-icon"></span>
   </button>
  <!-- Links -->
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" href="../pages/accueil.php">Accueil</a>
    </li>
    <?php
    $parentCategories = getActivatedCategories(getParentCategories());

    foreach($parentCategories as $parent){
        $childCategories = getActivatedCategories(getChildCategoriesOf($parent->getId()));
        if (count($childCategories) == 0) { // si catégorie seule
            echo "<li class=\"nav-item\">
            <a class=\"nav-link\" href=\"../pages/catalogue.php#categorie".$parent->getId()."\">".$parent->getNom()."</a>
            </li>";
        } else { //si catégorie avec sous menus
            echo "<li class=\"nav-item dropdown\">
            <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbardrop\" data-toggle=\"dropdown\">".$parent->getNom()."</a>
            <div class=\"dropdown-menu\">";
            foreach($childCategories as $child){
                echo "<a class=\"dropdown-item\" href=\"../pages/catalogue.php#categorie".$child->getId()."\">".$child->getNom()."</a>";
            }
            echo "</div>";
            echo "</li>";
        }
    }
    ?>

  
    <?php
        include("session.php");
        require_once("adminPageVerification.php");

        if(isAdmin()){
            echo '<li class="nav-item">
                    <a class="nav-link"  href="../pages/gestion.php">Gestion</a>
                  </li>';
        }
        if(isset($_SESSION['mail'])&&$_SESSION['mail']!=""){
            echo '<li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown"><i class=\'fas fa-user\' style=\'font-size:24px\'></i> '.$_SESSION['nom'].' '.$_SESSION['prenom'].'</a>
                    <div class="dropdown-menu">
                      <a class="dropdown-item" href="../pages/espacePerso.php">Mon espace</a>
                      <a class="dropdown-item" href="../traitement/disconnect.php">Déconnexion</a>
                    </div>
                  </li>';
        } else {
            echo '<li class="nav-item">
                    <a class="nav-link" href="#" data-toggle="modal" data-target="#myModal"><i class=\'fas fa-user\' style=\'font-size:24px\'></i> Connexion</a>
                  </li>';
        }
    ?>
    <li class="nav-item">
      <a class="nav-link" href="../pages/panier.php"><i class="fa fa-shopping-basket" style="font-size:24px;"></i> Panier</a>
    </li>
  </ul>
</div>
</nav>
<br>
<?php
    include("../forms/formConnexion.php");
?>
