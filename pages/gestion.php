<?php
    include("../includes/session.php");
    require_once("../includes/adminPageVerification.php");
    isAdmin(true);
?>
<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Les Bons Plants du Puech</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/stylesheet.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body>
    <?php
      require_once("../includes/nav.php");
    ?>
      <br><br><br>
      <div class="container-fluid">
		  <h2>Gestion du site</h2>

		  <ul class="nav nav-tabs">
			<li class="nav-item">
			  <a class="nav-link" href="#" onclick="changeMenu('gestion1')">Gestion des produits</a>
			</li>
			<li class="nav-item">
			  <a class="nav-link" href="#" onclick="changeMenu('gestion2')">Ajout d'un produit</a>
			</li>
			<li class="nav-item">
			  <a class="nav-link" href="#" onclick="changeMenu('gestion3')">Ajout d'une catégorie</a>
			</li>
			<li class="nav-item">
			  <a class="nav-link" href="#" onclick="changeMenu('gestion4')">Ajout d'une sous catégorie</a>
			</li>
			<li class="nav-item">
			  <a class="nav-link" href="#" onclick="changeMenu('gestion5')">Ajout d'une unité de prix</a>
			</li>
			<li class="nav-item">
			  <a class="nav-link" href="#" onclick="changeMenu('gestion6')">Modifier la page d'accueil</a>
			</li>
		  </ul>
		  
		  <div class="gestion" style="display:inline-block;" id="gestion1">
			<?php
			  include("../forms/formListeCat.php");
			?>
		  </div>
		  
		  <div class="gestion" id="gestion2">
			<?php
			  include("../forms/formAddProduct.php");
			?>
		  </div>
		  
		  <div class="gestion" id="gestion3">
			<?php
			  include("../forms/formAddCategorie.html");
			?>
		  </div>
		  
		  <div class="gestion" id="gestion4">
			<?php
			  include("../forms/formAddSousCategorie.php");
			?>
		  </div>
		  
		  <div class="gestion" id="gestion5">
			<?php
			  include("../forms/formAddUnite.html");
			?>
		  </div>
		  
		  <div class="gestion" id="gestion6">
			<?php
			  include("../forms/formUpdateAccueil.php");
			?>
		  </div>
		  
		</div>
    <?php
      require_once("../includes/footer.php");
    ?>
  </body>
</html>

<script src="../javascript/changeMenu.js"></script>
