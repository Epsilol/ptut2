<?php
require_once("../model/userFunctions.php");
require_once("../includes/session.php");
if(!isUserConnected($_SESSION["mail"],$_SESSION["nom"],$_SESSION["prenom"])){
  header("location: ../index.php");
}
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <title>Les Bons Plants du Puech</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="../css/stylesheet.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>

<body>
  <div>
    <?php
    require_once("../includes/nav.php");
    ?>
    <!-- Contenu -->
    <br><br><br>
    <div class="container-fluid">
      <h2>Mon espace Perso : <?php echo $_SESSION["nom"]." ".$_SESSION["prenom"];?></h2>
<br>
      <ul class="nav nav-tabs">
        <li class="nav-item">
          <a class="nav-link" href="#" onclick="changeMenu('menu1')">Modifier mon mot de passe</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#" onclick="changeMenu('menu2')">Modifier mon adresse</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#" onclick="changeMenu('menu3')">Modifier mon téléphone</a>
        </li>
      </ul>

      <div class="gestion" style="display:inline-block;" id="menu1">
        <?php
        include("../forms/formChangerMotDePasse.php");
        ?>
      </div>

      <div class="gestion" id="menu2">
        <?php
        include("../forms/formUpdateAddress.php");
        ?>
      </div>

      <div class="gestion" id="menu3">
        <?php
        include("../forms/formUpdateTel.php");
        ?>
      </div>
    </div>
    <?php
    require_once("../includes/footer.php");
    ?>
  </div>
</body>
</html>

<script src="../javascript/changeMenu.js"></script>
