<!DOCTYPE html>
<html>

<head>
 <meta charset="UTF-8">
 <title>Les Bons Plants du Puech</title>
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
 <link rel="stylesheet" href="../css/stylesheet.css">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>

<body>
	<div>
    <?php
		require_once("../includes/nav.php");
    require_once("../model/Cart.php");
    require_once("../model/produitFunctions.php");
    require_once("../model/userFunctions.php");
    ?>
	<!-- Contenu -->
	<div class="container-fluid">
        <br><br><br>
        <h2>Mon panier</h2>
        <div class="contenu">
          <hr>
        <?php
            //TODO remplacer par un formCart
            $panier = Cart::getInstance();
            $products = $panier->getProducts();
            $totalPrix = 0;
            echo "<div class=\"row\">";
              echo "<div class=\"col-8\">";
                echo "<div class=\"articleTitre\">Produit</div>";
              echo "</div>";
              echo "<div class=\"col-2\">";
                echo "<div class=\"prixTitre\">Prix</div>";
              echo "</div>";
            echo "</div>";
              echo "<hr>";
              echo "<form id=\"formValidPanier\" action=\"../traitement/sendPDF.php\" method=\"post\">";
            foreach($products as $id=>$quantity){
                $prod = getProduitById($id);
                echo "<div id=\"article".$id."\">";
                echo "<div class=\"row\">";
                  echo "<input type=\"hidden\" name=\"prod[]\" value=\"".$id."\">";
                  echo "<input type=\"hidden\" name=\"quant[]\" value=\"".$quantity."\">";
                  echo "<div class=\"col-8\">";
                    echo "<div class=\"article\"><img class=\"miniature\" src=\"../img/".$prod->getPhoto()."\" alt=\"".$prod->getNom()."\" style=\"width:100%\"> ".$prod->getNom()." : ".$quantity.' '.$prod->getUnite()->getLibelle();
                    echo "</div>";
                  echo "</div>";
                  echo "<div class=\"col-2\">";
                    $prix = $quantity * $prod->getPrix();
                    echo "<div class=\"prix\"><div class=\"prix\" id=\"prix".$id."\">".$prix ."</div>€ </div>";
                    $totalPrix += $quantity * $prod->getPrix();
                  echo "</div>";
                  echo "<div class=\"col-1\">";
                    echo "<a class=\"trash\" href=\"#\" onclick=\"suppArticlePanier(".$id.")\">X</a>";
                  echo "</div>";
                echo "</div>";
                  echo "<hr>";
                echo "</div>";
            }
            echo "<div class=\"total\">Prix total : <div class=\"prix\" id=\"total\"> ".$totalPrix."</div>€</div>";
            if (isUserConnected($_SESSION['mail'],$_SESSION['nom'],$_SESSION['prenom'])){
                echo "<input class=\"btn myBtn\" id=\"validPanier\" type=\"submit\" value=\"Valider Panier\">";
            } else {
                echo '<div class="messageConnecte">Veuillez vous connecter afin de finaliser votre panier.</div>';
            }
            echo "</form>";
        ?>
        </div>
    </div>
    <?php
      require_once("../includes/footer.php");
    ?>
	</div>
</body>
<script src="../javascript/suppArticlePanier.js"></script>
</html>
