<!DOCTYPE html>
<html>

<head>
 <meta charset="UTF-8">
 <title>Les Bons Plants du Puech</title>
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
 <link rel="stylesheet" href="../css/stylesheet.css">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body data-spy="scroll" data-target=".navbar" data-offset="50">
	<div>
    <?php
			require_once("../includes/nav.php");
		?>
	<!-- Contenu -->
		<div class="container-fluid">
      <br><br>
        <?php
        require_once("../model/produitFunctions.php");
        require_once("../model/categorieFunctions.php");
        require_once("../forms/formProduit.php");

        $categories = getActivatedCategories(getParentCategories());

        foreach($categories as $parent){
          echo "<div id=\"categorie".$parent->getId()."\">";
          echo "<hr>";
            echo "<h2>".$parent->getNom()."</h2>";
            //Affiche Produits
            $produits = getProduitsForCategorie($parent->getId());
            echo "<div class=\"row\">";
            foreach($produits as $produit){
              showProduct($produit);
            }
            echo "</div>";
            //Affiche Sous Categorie
            $childs = getActivatedCategories(getChildCategoriesOf($parent->getId()));
            foreach($childs as $child){
              echo "<hr>";
              echo "<div id=\"categorie".$child->getId()."\">";
              echo "<h4>".$child->getNom()."</h4>";
              //Affiche Produits
              $produitsFils = getActivatedProduits(getProduitsForCategorie($child->getId()));
              echo "<div class=\"row\">";
              foreach($produitsFils as $produit){
                showProduct($produit);
              }
              echo "</div>";
              echo "</div>";
            }
            //Affiche Produit
          echo "</div>";
        }
        ?>
    </div>
    <?php
      require_once("../includes/footer.php");
    ?>
	</div>
</div>
</body>
</html>
<script src="../javascript/addToCart.js"></script>
