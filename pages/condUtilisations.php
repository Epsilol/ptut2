<!DOCTYPE html>
<html>

<head>
 <meta charset="UTF-8">
 <title>Les Bons Plants du Puech</title>
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
 <link rel="stylesheet" href="../css/stylesheet.css">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>

<body>
	<div>
    <?php
			require_once("../includes/nav.php");
		?>
	<!-- Contenu -->
		<div class="container-fluid">
<br><br><br>
      <p>
<h1>Conditions d'Utilisation des Données
& Politique de Confidentialité</h1>

<h2>1 - PROTECTION DES DONNEES</h2>

Conformément à l'article 27 de la loi Informatique et libertés (n°78-17 du 6 janvier 1978), les clients du GAEC LES BONS PLANTS DU PUECH disposent du droit d'accès et de rectification de leurs données personnelles selon le moyen de leur choix (voir §3).<br>

Les données collectées par LES BONS PLANTS DU PUECH sont strictement nécessaires au bon traitement des commandes ou à la mise à disposition de services personnalisés.<br>

<h2>2 - MENTIONS LEGALES</h2>

<h2>3 – INFORMATIQUE ET LIBERTÉ</h2>

Conformément à la loi informatique et liberté du 06/01/78 (art. 27), vous disposez d'un droit d'accès et de rectification des données vous concernant. Utilisez pour cela notre formulaire de contact.

      </p>

    	</div>
    <?php
      require_once("../includes/footer.html");
    ?>
	</div>
</body>
</html>
