<?php
    include("../includes/session.php");
    require_once("../includes/adminPageVerification.php");
    isAdmin(true);
?>
<!DOCTYPE html>
<html>

<head>
 <meta charset="UTF-8">
 <title>Les Bons Plants du Puech</title>
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
 <link rel="stylesheet" href="../css/stylesheet.css">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>

<body>
	<div>
    <?php
			require_once("../includes/nav.php");
		?>
	<!-- Contenu -->
		<div class="container-fluid">
<br><br><br>
      <?php
          include("../forms/formEditProduit.php");
      ?>
    	</div>
    <?php
      require_once("../includes/footer.php");
    ?>
	</div>
</body>
</html>
