<!DOCTYPE html>
<html>

<head>
 <meta charset="UTF-8">
 <title>Les Bons Plants du Puech</title>
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
 <link rel="stylesheet" href="../css/stylesheet.css">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>

<body>
	<div>
        <?php
			require_once("../includes/nav.php");
		?>
	<!-- Contenu -->
		<div class="container-fluid">
        <br><br><br>
        <h2>Réinitialiser mon mot de passe</h2>
        <div>
          <form id="formAddCat" action="../traitement/resetToken.php" method="post">
            <label for="fname">Votre adresse mail :</label>
            <input class="form-control" type="email" id="emailForgot" name="mail" placeholder="Exemple : nom@domaine.com"><br>
            <input type="submit" class="btn myBtn" value="Demander nouveau mot de passe" >
          </form>

        </div>
        <br>

    	</div>
    <?php
      require_once("../includes/footer.php");
    ?>
	</div>
</body>
</html>
