<?php
    require_once("../model/userFunctions.php");
    require_once("../includes/session.php");
    if(!hasUserRequestedResetPassword($_GET["token"])){
        header("location: ../index.php");
    }
    if(!empty($_GET['token'])){
        disconnectUser();
    }
?>
<!DOCTYPE html>
<html>

<head>
 <meta charset="UTF-8">
 <title>Les Bons Plants du Puech</title>
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
 <link rel="stylesheet" href="../css/stylesheet.css">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>

<body>
	<div>
        <?php
			require_once("../includes/nav.php");
		?>
	<!-- Contenu -->
            <div class="container-fluid">
            <br><br><br>
            <h2>Changer mon mot de passe</h2>
            <form action="../traitement/changePassword.php" method="post">
            <?php
                echo '<input type="hidden" name="token" value="'.$_GET["token"].'"/>';
            ?>
              <label>Nouveau mot de passe *:</label>
              <input required class="form-control" type="password" id="password_" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" name="newPassword"><br>
              <label>Confirmer le nouveau mot de passe *:</label>
              <input required class="form-control" type="password" id="confirm_password_" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" name="confirm_password"><br>
              <input class="btn myBtn" type="submit" value="Valider">
            </form>
            * Le mot de passe doit faire au minimum 8 caractères, et doit contenir au minimum une majuscule, une minuscule, un chiffre et un caractère spécial.

          </div>
      <?php
      require_once("../includes/footer.html");
    ?>
	</div>
</body>
</html>
<script>
  var password = document.getElementById("password_");
  var confirm_password = document.getElementById("confirm_password_");

  function validatePassword(){
    if(password.value != confirm_password.value) {
      confirm_password.setCustomValidity("Les mots de passes ne correspondent pas.");
    } else {
      confirm_password.setCustomValidity('');
    }
  }

  password.onchange = validatePassword;
  confirm_password.onblur = validatePassword;
</script>
