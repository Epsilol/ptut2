<?php
    require_once("../model/userFunctions.php");
    $hashedNewPassword = password_hash($_POST["newPassword"], PASSWORD_BCRYPT);
    if(isset($_POST['password'])){
        changeUserPassword($_POST['mail'],$_POST["password"],$hashedNewPassword);
    } else if (isset($_POST['token'])) {
        changeUserPasswordWithToken($_POST['token'],$hashedNewPassword);
    }
    header("location: ../index.php");
?>