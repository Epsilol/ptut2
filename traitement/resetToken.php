<?php
    require_once('../model/userFunctions.php');
    
    require_once('../includes/phpmailerIncludes.php');
    use PHPMailer\PHPMailer\PHPMailer;
    $token = resetUserToken($_POST["mail"]);
    
    if ($token != null){
        $http = "<a href=\"http://".$_SERVER['HTTP_HOST'];
        if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off'){
            $link = "<a href=\"https://".$_SERVER['HTTPS_HOST'];;
        }
        $link = $http."/pages/changerMotDePasse.php?token=".$token."\">cliquez ici.</a>"; //change it to https if necessary
        $mailText = 'Bonjour,<br>'.
        'Une demande de réinitialisation de mot de passe a été initée à cette adresse mail.<br>'.
        'Vous pouvez changer votre mot de passe en vous rendant à l\'adresse suivante : '.$link.'<br>'.
        'Si vous n\'avez pas initié cette demande, vous pouvez ignorer ce message.<br>'.
        'Cordialement,<br>'.
        'L\'Équipe des bons plans du puech.';
        
        //Create a new PHPMailer instance
        $mail = new PHPMailer();
        $mail->CharSet = PHPMailer::CHARSET_UTF8;
        //Set who the message is to be sent from
        $mail->setFrom('no-reply@bonsplandupuech.com', 'no-reply admin');
        //Set an alternative reply-to address
        $mail->addReplyTo('no-reply@bonsplandupuech.com', 'no-reply admin');
        //Set who the message is to be sent to
        $user = getUserNomAndPrenom($_POST["mail"]);
        $mail->addAddress($_POST["mail"],$user["nom"].' '.$user["prenom"]);
        //Set the subject line
        $mail->Subject = "Réinitialiser le mot de de passe";
        //Read an HTML message body from an external file, convert referenced images to embedded,
        //convert HTML into a basic plain-text alternative body
        $mail->msgHTML($mailText, __DIR__);
        
         //send the message
        $mail->send();
    }
    
    header("location: ../index.php");
?>