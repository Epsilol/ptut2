<?php
  require_once("../model/categorieFunctions.php");
  include("../includes/session.php");
  require_once("../includes/adminPageVerification.php");
  
  if(isAdmin()){
    addCategorie($_POST["ssCatName"], $_POST["CatMaitre"]);
  }
  
  header('location: ../pages/gestion.php');
?>
