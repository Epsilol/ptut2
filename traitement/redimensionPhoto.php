<?php
// ---------------------------------------------------
// Fonction de REDIMENSIONNEMENT physique "PROPORTIONNEL" et ENREGISTEMENT
// ---------------------------------------------------
// retourne : true si le REDIMENSIONNEMENT et l'ENREGISTEMENT ont bien eu lieu, sinon false
// ---------------------
// La FONCTION : fct_img_redim_ratio ($W_max, $H_max, $rep_Dst, $img_Dst, $rep_Src, $img_Src)
// Les paramètres :
// - $W_max : LARGEUR maxi finale --> ou 0
// - $H_max : HAUTEUR maxi finale --> ou 0
// - $rep_Dst : répertoire de l'image de Destination (déprotégé) --> ou '' (même répertoire)
// - $img_Dst : NOM de l'image de Destination --> ou '' (même nom que l'image Source)
// - $rep_Src : répertoire de l'image Source (déprotégé)
// - $img_Src : NOM de l'image Source
// ---------------------
// 3 options :
// A- si $W_max != 0 et $H_max != 0 : a LARGEUR maxi ET HAUTEUR maxi fixes
// B- si $H_max != 0 et $W_max == 0 : image finale a HAUTEUR maxi fixe (largeur auto)
// C- si $W_max == 0 et $H_max != 0 : image finale a LARGEUR maxi fixe (hauteur auto)
// Si l'image Source est plus petite que les dimensions indiquées : PAS de REDIMENSIONNEMENT.
// ---------------------
// UTILISATION (exemple) :
// $redimOK = fct_img_redim_ratio(120,80,'reppicto/','monpicto.jpg','repimage/','monimage.jpg');
// if( $redimOK == =true ){ echo 'Redimensionnement OK !'; }
// ---------------------------------------------------
function fct_img_redim_ratio( $W_max, $H_max, $rep_Dst, $img_Dst, $rep_Src, $img_Src )
{
    $condition = 0;
    // Si certains paramètres ont pour valeur '' :
    if( $rep_Dst == '' ){ $rep_Dst = $rep_Src; } // (même répertoire)
    if( $img_Dst == '' ){ $img_Dst = $img_Src; } // (même nom)
    // ---------------------
    // si le fichier existe dans le répertoire, on continue...
    if( file_exists($rep_Src.$img_Src) && ($W_max != 0 || $H_max != 0) )
    {
        // ----------------------
        // extensions acceptées
        $extension_allowed = array('jpg','jpeg','png','gif');
        // extension fichier Source
        $extension = strtolower( pathinfo($img_Src, PATHINFO_EXTENSION) );
        // extension OK ? on continue ...
        if( in_array($extension, $extension_allowed) )
        {
            // ------------------------
            // récupération des dimensions de l'image Src
            $img_size = getimagesize($rep_Src.$img_Src);
            $W_Src = $img_size[0]; // largeur
            $H_Src = $img_size[1]; // hauteur
            // ------------------------------------------------
            // condition de REDIMENSIONNEMENT et dimensions de l'image finale
            // ------------------------------------------------
            // A- LARGEUR ET HAUTEUR maxi fixes
            if( $W_max != 0 && $H_max != 0 ){
                $ratio_X = $W_Src / $W_max; // ratio en largeur
                $ratio_Y = $H_Src / $H_max; // ratio en hauteur
                $ratio = max($ratio_X,$ratio_Y); // le plus grand
                $W = $W_Src/$ratio;
                $H = $H_Src/$ratio;
                $condition = ($W_Src>$W) || ($H_Src>$H); // 1 si vrai (true)
            // B- HAUTEUR maxi fixe
            } elseif( $W_max == 0 && $H_max != 0 ){
                $H = $H_max;
                $W = $H * ($W_Src / $H_Src);
                $condition = ($H_Src > $H_max); // 1 si vrai (true)
            // C- LARGEUR maxi fixe
            } elseif( $W_max != 0 && $H_max == 0 ){
                $W = $W_max;
                $H = $W * ($H_Src / $W_Src);
                $condition = ($W_Src > $W_max); // 1 si vrai (true)
            }
            // ------------------------
            if( $condition != 1)  // mêmes dimensions
            {
                $W = $W_Src;
                $H = $H_Src;
                $condition = 1; // vrai (true) -> ON FORCE ici la condition = on oblige le redimensionnement/enregistrement
            }

            // ------------------------------------------------
            // REDIMENSIONNEMENT
            // ------------------------------------------------
            if( $condition == 1)
            {
                // ---------------------
                // création de la ressource-image "Src" en fonction de l extension
                switch($extension ){
                case 'jpg':
                case 'jpeg':
                    $Ress_Src = imagecreatefromjpeg($rep_Src.$img_Src);
                    break;
                case 'png':
                    $Ress_Src = imagecreatefrompng($rep_Src.$img_Src);
                    break;
                case 'gif':
                    $Ress_Src = imagecreatefromgif($rep_Src.$img_Src);
                  break;
                }
                // ---------------------
                // création d une ressource-image "Dst" aux dimensions finales
                // fond noir (par défaut)
                switch($extension ){
                case 'jpg':
                case 'jpeg':
                case 'gif':
                    $Ress_Dst = imagecreatetruecolor($W,$H);
                    break;
                case 'png':
                    $Ress_Dst = imagecreatetruecolor($W,$H);
                    // fond transparent (pour les png avec transparence)
                    imagesavealpha($Ress_Dst, true);
                    $trans_color = imagecolorallocatealpha($Ress_Dst, 0, 0, 0, 127);
                    imagefill($Ress_Dst, 0, 0, $trans_color);
                    break;
                }
                // ------------------------------------------------
                // REDIMENSIONNEMENT (copie, redimensionne, rééchantillonne)
                imagecopyresampled($Ress_Dst, $Ress_Src, 0, 0, 0, 0, $W, $H, $W_Src, $H_Src);
                // ------------------------------------------------
                $imgJPG_Quality = 75;    // 0=nul / 75=moyen / 100=meilleur (mais image plus lourde)
                $imgPNG_Quality = 7;    // 0=nul / 7=moyen / 9=meilleur (mais image plus lourde)
                // ENREGISTREMENT dans le répertoire (avec la fonction appropriée)
                switch ($extension ){
                case 'jpg':
                case 'jpeg':
                    imagejpeg ($Ress_Dst, $rep_Dst.$img_Dst, $imgJPG_Quality);
                    break;
                case 'png':
                    imagepng ($Ress_Dst, $rep_Dst.$img_Dst, $imgPNG_Quality);
                    break;
                case 'gif':
                    imagegif ($Ress_Dst, $rep_Dst.$img_Dst );
                    break;
                }
                // ------------------------
                // libération des ressources-image
                imagedestroy ($Ress_Src);
                imagedestroy ($Ress_Dst);
                // ------------------------
            } else {
                // ------------------------------------------------
                // PAS de redimensionnement
                if( $rep_Src.$img_Src != $rep_Dst.$img_Dst ){
                    // On COPIE le fichier TEL QUEL
                    copy($rep_Src.$img_Src, $rep_Dst.$img_Dst);
                }
                // ------------------------------------------------
            }
        }
    }
    // -----------------------------------------------
    // retourne : true si le REDIMENSIONNEMENT et l'ENREGISTEMENT ont bien eu lieu, sinon false
    if( $condition == 1 && file_exists($rep_Dst.$img_Dst) ){ return true; }
    else { return false; }
    // -----------------------------------------------
};
?>
