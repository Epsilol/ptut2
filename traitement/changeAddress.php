<?php
    require_once("../model/userFunctions.php");
    require_once("../includes/session.php");
    if(isset($_POST['adresse'])){
        changeUserAddress($_SESSION["mail"],htmlspecialchars($_POST["adresse"]));
    }
    header("location: ../index.php");
?>