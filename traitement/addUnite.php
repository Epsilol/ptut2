<?php
  require_once("../model/uniteFunctions.php");
  include("../includes/session.php");
  require_once("../includes/adminPageVerification.php");
  
  if(isAdmin()){
    addUnite($_POST["nameUnit"]);
  }
  header('location: ../pages/gestion.php');
?>
