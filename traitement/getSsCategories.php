<?php
  require_once("../model/categorieFunctions.php");
  include("../includes/session.php");
  require_once("../includes/adminPageVerification.php");

  if(isAdmin()){
      echo "<OPTION value=\"null\">Aucune</OPTION>";
      $childs = getChildCategoriesOf($_POST["idMaitre"]);
      foreach($childs as $child){
          echo "<OPTION value=".$child->getId().">".$child->getNom()."</OPTION>";
      }
  }
?>
