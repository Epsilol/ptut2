<?php
  require_once("../model/categorieFunctions.php");
  include("../includes/session.php");
  require_once("../includes/adminPageVerification.php");
  // Prépare une requête pour l'exécution
  
  if(isAdmin()){
    addCategorie($_POST["catName"]);
  }
  
  header('location: ../pages/gestion.php');
?>
