<?php
require_once("redimensionPhoto.php");

// Gérer upload Photo
function uploadPhoto($idProduct) {
    $photo = null;
    $tabExt = array('jpg','gif','png','jpeg');
    if(!empty($_FILES['image']['name'])){
        $extension = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);

        if(in_array(strtolower($extension),$tabExt)){

          //Path
          $dossier = '../img/';
          if (!is_dir($dossier)){
              mkdir($dossier);
          }

            $oldNameFichier = explode('.', basename($_FILES['image']['name']));
            $fichier = $idProduct;

            $fichier .= '.'.$oldNameFichier[1];
            if(move_uploaded_file($_FILES['image']['tmp_name'], $dossier . $fichier)){
                $redimOK = fct_img_redim_ratio(300, 500, $dossier, $fichier, $dossier, $fichier );
                if( $redimOK == 1 ){ echo 'Redimensionnement OK !'; }
                $photo = $fichier;
            } else {
              //Pas pu déplacé la photo
            }
        } else {
          //Extension non reconnu
        }
      } else {
        //Pas de photo upload
        $photo = "default.jpg";
      }
      return $photo;
}
?>
