<?php
  require_once("../model/userFunctions.php");

  $hashedPassword = password_hash($_POST["password"], PASSWORD_BCRYPT);
  addUser($_POST["mail"], $hashedPassword, $_POST["nom"], $_POST["prenom"], $_POST["adresse"],$_POST["telephone"]);
  
  header('location: ../pages/accueil.php');
?>
