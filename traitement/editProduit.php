<?php
  require_once("uploadPhoto.php");
  require_once("../model/produitFunctions.php");
  include("../includes/session.php");
  require_once("../includes/adminPageVerification.php");
  
  // //Exécute la requête préparée.
  if(isAdmin()){
      $categorie = ($_POST["ssCategorie"] == "null") ? $_POST["catMaitre"] : $_POST["ssCategorie"];
      $prix = str_replace(',', '.', $_POST["productPrice"]);
      if(!empty($_FILES['image']['name'])){
          updateProduit($_POST["idProduit"],$_POST["productName"], "default.jpg", $_POST["descProduit"], $prix, $_POST["unitProduct"], $categorie);
          $addedProduit = getProduitByNom($_POST["productName"]);
          $photo = uploadPhoto($addedProduit->getId());
          setProduitPhoto($addedProduit->getId(),$photo);
      } else {
          updateProduitNoPhoto($_POST["idProduit"],$_POST["productName"], $_POST["descProduit"], $prix, $_POST["unitProduct"], $categorie);
      }
  }
  
  header('location: ../pages/gestion.php');
?>