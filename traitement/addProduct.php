<?php
  require_once("uploadPhoto.php");
  require_once("../model/produitFunctions.php");
  include("../includes/session.php");
  require_once("../includes/adminPageVerification.php");
  
  // //Exécute la requête préparée.
  if(isAdmin()){
      $categorie = ($_POST["ssCategorie"] == "null") ? $_POST["catMaitre"] : $_POST["ssCategorie"];
      $prix = str_replace(',', '.', $_POST["productPrice"]);
      addProduit($_POST["productName"], NULL, $_POST["descProduit"], $prix, $_POST["unitProduct"], $categorie);
      $addedProduit = getProduitByNom($_POST["productName"]);
      $photo = uploadPhoto($addedProduit->getId());
      setProduitPhoto($addedProduit->getId(),$photo);
  }
  
  header('location: ../pages/gestion.php');
?>
