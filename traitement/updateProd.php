<?php
require_once("../model/categorieFunctions.php");
require_once("../model/produitFunctions.php");

include("../includes/session.php");
require_once("../includes/adminPageVerification.php");

function verifyCategories($categories,$checkedCategories,$checkedProduits){
    foreach ($categories as $categorie){
      if (in_array($categorie->getId(),$checkedCategories)){
          setCategorieState($categorie->getId(),true);
          verifyCategories(getChildCategoriesOf($categorie->getId()),$checkedCategories,$checkedProduits);
      } else {
          desactivateCategorieAndChilds($categorie);
          continue;
      }
      foreach(getProduitsForCategorie($categorie->getId()) as $produit){
          if(in_array($produit->getId(),$checkedProduits)){
              setProduitState($produit->getId(),1);              
          } else {
              setProduitState($produit->getId(),0);
          }
      }
    }
}

function desactivateCategorieAndChilds($categorie){
    foreach(getChildCategoriesOf($categorie->getId()) as $child){
        desactivateCategorieAndChilds($child);
    }
    setCategorieState($categorie->getId(),0);
    foreach(getProduitsForCategorie($categorie->getId()) as $produit){
        setProduitState($produit->getId(),0);
    }
}

  //TODO tout mettre en désactivé
  if(isAdmin()){
      verifyCategories(getParentCategories(),$_POST["cat"],$_POST["prod"]);
  }

  header('location: ../pages/gestion.php');
?>
