<?php
    include("../includes/session.php");
    require_once("../model/userFunctions.php");
    try{
        $user = tryConnectUser($_POST["mail"],$_POST["password"]);
        $_SESSION['mail'] = $user->getMail();
        $_SESSION['isAdmin'] = $user->isAdmin();
        $_SESSION['nom'] = $user->getNom();
        $_SESSION['prenom'] = $user->getPrenom();
    } catch(Exception $e) {
        
    }
    
    header("location: ..".$_POST["page"]);
?>
