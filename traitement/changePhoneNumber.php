<?php
    require_once("../model/userFunctions.php");
    require_once("../includes/session.php");
    if(isset($_POST['telephone'])){
        changeUserPhoneNumber($_SESSION["mail"],htmlspecialchars($_POST["telephone"]));
    }
    header("location: ../index.php");
?>