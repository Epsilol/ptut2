<?php
	require_once("../model/PDF.php");
	require_once("../model/produitFunctions.php");
	require_once("../model/userFunctions.php");
	require_once("../model/Cart.php");
	include("../includes/session.php");
    require_once("../includes/phpmailerIncludes.php");
    
    use PHPMailer\PHPMailer\PHPMailer;
	
	$idProd = $_POST["prod"];
    if(!empty($_POST["prod"]) && isUserConnected($_SESSION['mail'],$_SESSION['nom'],$_SESSION['prenom'])){
        $prod = array();
        $quantité = $_POST["quant"];
        $totalPrix = 0;
        
        // Instanciation de la classe dérivée
        $pdf = new PDF();
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetFont('Arial','',12);
        $w = array(150,40);
        $i = 0; 
		$ini = parse_ini_file('../config/owner_informations.ini');
		
		$pdf->Cell($w[0],6,"Commande de : ".utf8_decode($_SESSION['nom'])."  ".utf8_decode($_SESSION['prenom']),0,0,'LR');
		
		$pdf->Ln();
		$pdf->Cell($w[0],6,"Mail : ".utf8_decode($_SESSION['mail']),0,0,'LR');
		$pdf->Ln();
		$pdf->Cell($w[0],6,utf8_decode("Téléphone : ".getUserAddressAndPhoneNumber($_SESSION['mail'])["phoneNumber"]),0,0,'LR');
		$pdf->Ln();
		$pdf->Cell($w[0],6,"Adresse : ".utf8_decode(getUserAddressAndPhoneNumber($_SESSION['mail'])["address"]),0,0,'LR');
		$pdf->Ln(20);
		
		$pdf->Cell($w[0],6,utf8_decode("Récuperez votre commande à l'adresse suivante : ".$ini["owner_address"]),0,0,'LR');
		$pdf->Ln();
		$pdf->Cell($w[0],6,utf8_decode("Contactez le : ".$ini["owner_tel"]),0,0,'LR');
		$pdf->Ln(20);
		
        foreach($idProd as $id){
            $pdf->Cell($w[0],6,utf8_decode(getProduitById($id)->getNom())." x ".$quantité[$i],1,0,'LR');
            $pdf->Cell($w[1],6," Prix : ".(getProduitById($id)->getPrix()*$quantité[$i])." ".chr(128),1,0,'R');
            $pdf->Ln();
            $totalPrix += getProduitById($id)->getPrix()*$quantité[$i];
            $i++;
        }
        $pdf->Cell($w[0],6,'',0,0,'R');
        $pdf->Cell($w[1],6,"Total : ".$totalPrix." ".chr(128),1,0,'R');
        
        if(!is_dir('../tmp/')){
            mkdir('../tmp/');
        }
        
        $unwanted_array = array(    'Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
                                'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
                                'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
                                'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
                                'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y' );
        $prenom = strtr( $_SESSION['prenom'], $unwanted_array );
        $nom = strtr( $_SESSION['nom'], $unwanted_array );

        $pdfFile = "../tmp/".$nom."_".$prenom.".pdf";
        $pdf->Output("F",$pdfFile,true);
        
        $mail = new PHPMailer();
        
        $mailText = 'Bonjour,<br>'.
        'Nous vous informons que vous avez bien réalisé une commande sur notre site.<br>'.
        'Vous trouverez ci-joint un fichier pdf affichant le contenu de votre commande.<br>'.
        'Cordialement,<br>'.
        'L\'Équipe des bons plans du puech.';
        
        $ini = parse_ini_file('../config/owner_informations.ini');
        
        try {
            $mail->isMail();
            $mail->CharSet = PHPMailer::CHARSET_UTF8;
            $mail->setFrom('no-reply@lesbonsplansdupuech.com', 'no-reply admin');
            $mail->addAddress($_SESSION['mail'], $_SESSION['prenom'].' '.$_SESSION['nom']);
            $mail->addCC($ini['owner_mail'], $ini['owner_firstname'].' '.$ini['owner_surname']);
            $mail->Subject = 'Votre Panier';
            $mail->msgHTML($mailText);
            $mail->addAttachment($pdfFile);
            if ($mail->send()){
                $cart = Cart::getInstance();
                $cart->clean();
                //unlink($pdfFile);
            }
        } catch (Exception $e){
        }
        header('location: ../pages/commandeValid.php');
    } else {
        header('location: ../pages/accueil.php');
    }
    
?>