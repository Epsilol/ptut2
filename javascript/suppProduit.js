function suppProduit(data) {
  var str = "Voulez vous vraiment supprimer ce produit ?";

  if(confirm(str)){
    var node = document.getElementById("prod"+data);
    node.remove();

    var xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
      }
    };
    xhttp.open("POST", "../traitement/suppProduit.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("produit="+data);
  }
}
