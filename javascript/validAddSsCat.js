function validAddSsCat(){
  var success = true;

  var ssCat = document.getElementById("nameAddSsCat");
  if(ssCat.value == ""){
    success = false;
    ssCat.setCustomValidity("Veuillez remplir ce champ.");
  } else {
    ssCat.setCustomValidity('');
  }

  if(success){
     document.getElementById("formAddSsCat").submit();
  }
}
