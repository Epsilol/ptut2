function addToCart(id) {
  var button = document.getElementById("button"+id)
    var xhttp = new XMLHttpRequest();
    var quantity = document.getElementById("prod"+id+"quantity").value;
    if(quantity > 0){
      xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          var messageAjout = "Produit Ajouté";
          var p = document.createElement("div");
          p.classList.add("messageAjout");
          p.innerHTML =  messageAjout;
          var referenceNode = button.parentNode;
          referenceNode.insertBefore(p, button.nextSibling);
          setTimeout(function(){p.remove();}, 2000);
        }
      };
      xhttp.open("POST", "../traitement/ajouterPanier.php", true);
      xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      xhttp.send("id="+id+"&quantity="+quantity);
    }
}
