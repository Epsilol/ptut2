function validAddProduct(){
  var success = true;
  var cat = document.getElementById("catAddProd");
  if(cat.value == "null"){
    success = false;
    cat.setCustomValidity('Veuillez choisir une catégorie.');
  } else {
    cat.setCustomValidity('');
  }

  var produit = document.getElementById("productName");
  var validityState_object = produit.validity;
  if(validityState_object.valueMissing){
    success = false;
    produit.setCustomValidity("Veuillez remplir ce champ.");
  } else {
    produit.setCustomValidity('');
  }

  var prix = document.getElementById("productPrice");
  var validityState_object = prix.validity;
  if(validityState_object.valueMissing){
    success = false;
    prix.setCustomValidity("Veuillez remplir ce champ.");
  } else {
    if(validityState_object.patternMismatch){
      success = false;
      prix.setCustomValidity("Veuillez mettre un entier ou un réel à deux décimales. Et utiliser la virgule comme séparateur.");
    } else {
      prix.setCustomValidity('');
    }
  }

  if(success){
    document.getElementById("formAddProduct").submit();
  }
}
