function validAddCat(){
  var success = true;

  var cat = document.getElementById("nameAddCat");
  if(cat.value == ""){
    success = false;
    cat.setCustomValidity("Veuillez remplir ce champ.");
  } else {
    cat.setCustomValidity('');
  }

  if(success){
     document.getElementById("formAddCat").submit();
  }
}
