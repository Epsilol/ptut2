function validAddunite(){
  var success = true;

  var unit = document.getElementById("nameAddUnit");
  if(unit.value == ""){
    success = false;
    unit.setCustomValidity("Veuillez remplir ce champ.");
  } else {
    unit.setCustomValidity('');
  }

  if(success){
    document.getElementById("formAddUnite").submit();
  }
}
