function suppCat(data) {
  var str = "Voulez vous vraiment supprimer cette catégorie ?";

  if(confirm(str)){
    var xhttp = new XMLHttpRequest();
    var node = document.getElementById("cat"+data);
    node.parentNode.remove();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
      }
    };
    xhttp.open("POST", "../traitement/suppCategorie.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("categorie="+data);
  }
}
