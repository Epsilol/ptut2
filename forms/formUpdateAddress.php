<div class="container-fluid">
<br>  <h3>Modifier son Adresse</h3>
  <form action="../traitement/changeAddress.php" enctype="multipart/form-data" method="post">

    <label>Votre adresse : </label>
    <?php
      require_once('../includes/session.php');
      $data = getUserAddressAndPhoneNumber($_SESSION["mail"]);
      $address = $data["address"];
    ?>
  	<input maxlength="100" class="form-control" type="text" name="adresse" value="<?php echo $address;?>" required> <br>

    <input class="btn myBtn" type="submit"  value="Valider"><br><br>
  </form>
</div>
