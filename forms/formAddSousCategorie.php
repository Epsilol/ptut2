<div>
  <h3>Ajouter une sous catégorie</h3>
  <form id="formAddSsCat" action="../traitement/addSousCategorie.php" method="post">
    <label for="fname">Sélectionner la catégorie : </label>
    <SELECT class="form-control" name="CatMaitre">
    <?php
        require_once("../model/categorieFunctions.php");
        $parents = getParentCategories();
        foreach($parents as $parent){
          echo "<OPTION value=".$parent->getId().">".$parent->getNom()."</OPTION>";
        }
    ?>
    </SELECT> <br>
    <label for="fname">Nom de la sous catégorie : </label>
    <input class="form-control" type="text" id="nameAddSsCat" maxlength="50" name="ssCatName" placeholder="Exemple : Tomates ..."><br>
  </form>
  <button class="btn myBtn" onclick="validAddSsCat()" >Créer la sous catégorie</button>
</div>
<script src="../javascript/validAddSsCat.js"></script>
