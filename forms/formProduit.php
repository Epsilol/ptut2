<?php
function showProduct($product){
    echo "<div class=\"col-md-4 col-xs-6 col-lg-3\">";
      echo "<div class=\"card\">";
        echo "<img class=\"card-img-top\" src=\"../img/".$product->getPhoto()."\" alt=\"".$product->getNom()."\" style=\"width:100%\">";
        echo "<div class=\"card-body\">";
        echo "<h4 class=\"card-title\">".$product->getNom()."</h4>";
        echo "<p class=\"card-text\"> Description : ".$product->getDescription()."</p>";
        $prix = str_replace('.', ',', $product->getPrix());
        echo "<p class=\"card-text\"> Prix : ".$prix." €/".$product->getUnite()->getLibelle()."</p>";
        echo "<input style=\"width:50%;\" type=\"number\" id=\"prod".$product->getId()."quantity\" name=\"quantity\" min=\"0\">";
        echo " <button id=\"button".$product->getId()."\" type=\"button\" onClick=\"addToCart(".$product->getId().")\"><i class=\"fa fa-shopping-basket\" style=\"font-size:24px;\"></i></button> ";
      echo "</div>";
    echo "</div>";
    echo "</div>";
}
?>
