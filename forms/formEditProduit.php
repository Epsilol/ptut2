<?php
require_once("../model/categorieFunctions.php");
require_once("../model/produitFunctions.php");
  $produit = getProduitById($_GET["prod"]);
?>
<div>
  <h3>Modifier un produit</h3>
  <form id="formAddProduct" action="../traitement/editProduit.php" enctype="multipart/form-data" method="post">
  <?php
    echo '<input type="hidden" name="idProduit" value="'.$_GET["prod"].'"/>';
  ?>
    <label>Sélectionner la catégorie : </label>
    <SELECT class="form-control" name="catMaitre" onchange="getSsCategories(this)" id="catAddProd">
    <?php
        require_once("../model/categorieFunctions.php");
        $parents = getParentCategories();
        $categorie = '';
        foreach($parents as $parent){
          if(($produit->getCategorie() == $parent)||($produit->getCategorie()->getCategorieMaitre() == $parent)){
            $categorie = $parent->getId();
            echo "<OPTION selected value=".$parent->getId()."> ".$parent->getNom()."</OPTION>";
          } else {
            echo "<OPTION value=".$parent->getId().">".$parent->getNom()."</OPTION>";
          }

        }
    ?>
  </SELECT> <br>
  <label>Sélectionner la sous catégorie : </label>
    <SELECT class="form-control" name="ssCategorie" id="ssCategorie">
      <OPTION value="null">Aucune</option>
      <?php
          require_once("../model/categorieFunctions.php");
          $childs = getChildCategoriesOf($categorie);
          foreach($childs as $child){
            if($child == $produit->getCategorie()){
              echo "<OPTION selected value=".$child->getId().">".$child->getNom()."</OPTION>";
            } else {
              echo "<OPTION value=".$child->getId().">".$child->getNom()."</OPTION>";
            }
          }
      ?>
    </SELECT>
      <br>
    <label>Nom du produit : </label>
    <input class="form-control" maxlength="50" required type="text" id="productName" name="productName" placeholder="Exemple : Coeur de Boeuf ..." value="<?php echo $produit->getNom();?>"><br>
    <label>Description: </label>
    <textarea class="form-control" maxlength="500" type="texte" name="descProduit" rows="5" cols="100" ><?php echo $produit->getDescription();?></textarea><br>
    <label>Prix Unitaire : </label>
    <?php
      $prix = str_replace('.', ',', $produit->getPrix());
     ?>
    <input type="texte" required id="productPrice" name="productPrice" placeholder="Exemple : 2,48" pattern="\d+(,\d{2})?" value="<?php echo $prix; ?>"> €/
    <SELECT name="unitProduct">
      <?php
          require_once("../model/uniteFunctions.php");
          $unites = getUnites();
          foreach($unites as $unite){
            if($produit->getUnite() == $unite){
                echo "<OPTION selected value=".$unite->getId().">".$unite->getLibelle()."</OPTION>";
            } else {
                echo "<OPTION value=".$unite->getId().">".$unite->getLibelle()."</OPTION>";
            }
          }
      ?>
    </SELECT>
     <br>
     <label>Photo : </label>
     <?php
          if($produit->getPhoto() != "default.jpg"){
            echo "<img class=\"miniature\" src=\"../img/".$produit->getPhoto()."\" alt=\"".$produit->getNom()."\" style=\"width:100%\">";
          } else {
            echo "Pas de photo";
          }

     ?>
     <input type="file" name="image" accept="image/png, image/jpeg"><br>
  </form>
    <button class="btn myBtn" onclick="validAddProduct()" >Modifier le produit</button>
    <button class="btn myBtn"><a href="../pages/gestion.php">Retour</a></button>
</div>
<script src="../javascript/getSsCategorie.js"></script>
<script src="../javascript/validAddProduct.js"></script>
