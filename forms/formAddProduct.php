<div>
  <h3>Ajouter un produit</h3>
  <form id="formAddProduct" action="../traitement/addProduct.php" enctype="multipart/form-data" method="post">
    <label>Sélectionner la catégorie : </label>
    <SELECT class="form-control" name="catMaitre" onchange="getSsCategories(this)" id="catAddProd">
      <OPTION value="null">--Sélectionner une catégorie--</OPTION>
    <?php
        require_once("../model/categorieFunctions.php");
        $parents = getParentCategories();
        foreach($parents as $parent){
          echo "<OPTION value=".$parent->getId().">".$parent->getNom()."</OPTION>";
        }
    ?>
  </SELECT> <br>
  <label>Sélectionner la sous catégorie : </label>
    <SELECT class="form-control" name="ssCategorie" id="ssCategorie">
      <OPTION value="null">Aucune</OPTION>
    </SELECT>
      <br>
    <label>Nom du produit : </label>
    <input class="form-control" maxlength="50" required type="text" id="productName" name="productName" placeholder="Exemple : Coeur de Boeuf ..."><br>
    <label>Description: </label>
    <textarea class="form-control" maxlength="500" type="texte" name="descProduit" rows="5" cols="100" ></textarea><br>
    <label>Prix Unitaire : </label>
    <input type="texte" required id="productPrice" name="productPrice" placeholder="Exemple : 2,48" pattern="\d+(,\d{2})?"> €/
    <SELECT name="unitProduct">
      <?php
          require_once("../model/uniteFunctions.php");
          $unites = getUnites();
          foreach($unites as $unite){
            echo "<OPTION value=".$unite->getId().">".$unite->getLibelle()."</OPTION>";
          }
      ?>
    <?php
    ?>
    </SELECT>
     <br>
     <label>Photo : </label>
     <input type="file" name="image" accept="image/png, image/jpeg"><br>
  </form>
    <button class="btn myBtn" onclick="validAddProduct()" >Ajouter un produit</button>
</div>
<script src="../javascript/getSsCategorie.js"></script>
<script src="../javascript/validAddProduct.js"></script>
