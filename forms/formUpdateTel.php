<div class="container-fluid"><br>
  <h3>Modifier son Telephone</h3>
  <form action="../traitement/changePhoneNumber.php" enctype="multipart/form-data" method="post">

    <label>Votre numéro de téléphone : </label>
    <?php
      require_once('../includes/session.php');
      $data = getUserAddressAndPhoneNumber($_SESSION["mail"]);
      $tel = $data["phoneNumber"];
    ?>
    <input maxlength="15"class="form-control" type="text" name="telephone" value="<?php echo $tel;?>" required><br>

    <input class="btn myBtn" type="submit"  value="Valider"><br><br>
  </form>
</div>
