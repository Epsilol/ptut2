<div>
  <h3>Activer un produit</h3>
  <form action="../traitement/updateProd.php" method="post">
    <label>Catégorie - Sous Catégorie - Produit - Actif - Supprimer</label>
    <?php
      require_once("../model/categorieFunctions.php");
      require_once("../model/produitFunctions.php");

      function listProductsOf($idCategorie){
          $produits = getProduitsForCategorie($idCategorie);
          foreach($produits as $produit){
            echo "<div class=\"liste_produit\">";
            if($produit->isActif()){
                echo "<div id=\"prod".$produit->getId()."\" class=\"displayProd\">".$produit->getNom()." - <input name=\"prod[]\" type=\"checkbox\" checked value=".$produit->getId()."> - <a href=\"editProd.php?prod=".$produit->getId()."\"><i class=\"fa fa-edit edit\"></i></a>- <a href=\"#\" onclick=\"suppProduit(".$produit->getId().")\"><div class=\"trash\">X</div></a> </div>";
            } else {
                echo "<div id=\"prod".$produit->getId()."\" class=\"displayProd\">".$produit->getNom()." - <input name=\"prod[]\" type=\"checkbox\" value=".$produit->getId()."> - <a href=\"editProd.php?prod=".$produit->getId()."\"><i class=\"fa fa-edit edit\"></i></a>- <a href=\"#\" onclick=\"suppProduit(".$produit->getId().")\"><div class=\"trash\">X</div></a></div>";
            }
            echo "</div>";
          }
      }

      //Get Categorie
      $parents = getParentCategories();

      foreach($parents as $parent){
        echo "<div class=\"liste_categorie\">";
        if($parent->isActif()){
          echo "<div id=\"cat".$parent->getId()."\" class=\"displayCategorie\">".$parent->getNom()." - <input name=\"cat[]\" type=\"checkbox\" checked value=".$parent->getId()."> - <a href=\"#\" onclick=\"suppCat(".$parent->getId().")\"><div class=\"trash\">X</div></a></div>";
        } else {
          echo "<div id=\"cat".$parent->getId()."\" class=\"displayCategorie\">".$parent->getNom()." - <input name=\"cat[]\" type=\"checkbox\" value=".$parent->getId()."> - <a href=\"#\" onclick=\"suppCat(".$parent->getId().")\"><div class=\"trash\">X</div></a></div>";
        }
        echo "<div class=\"liste_ssCat\">";
          listProductsOf($parent->getId());
        echo "</div>";
          $childs = getChildCategoriesOf($parent->getId());
          foreach($childs as $child){
            echo "<div class=\"liste_ssCat\">";
            if($child->isActif()){
              echo "<div id=\"cat".$child->getId()."\" class=\"displaySsCategorie\">".$child->getNom()." - <input name=\"cat[]\" type=\"checkbox\" checked value=".$child->getId()."> - <a href=\"#\" onclick=\"suppCat(".$child->getId().")\"><div class=\"trash\">X</div></a></div>";
            } else {
              echo "<div id=\"cat".$child->getId()."\" class=\"displaySsCategorie\">".$child->getNom()." - <input name=\"cat[]\" type=\"checkbox\" value=".$child->getId()."> - <a href=\"#\" onclick=\"suppCat(".$child->getId().")\"><div class=\"trash\">X</div></a></div>";
            }
            listProductsOf($child->getId());
            echo "</div>";
          }

        echo "</div>";
      }
    ?>
    <input class="btn myBtn" type="submit" value="Valider"><br><br>
  </form>
</div>
 <script src="../javascript/suppCat.js"></script>
 <script src="../javascript/suppProduit.js"></script>
