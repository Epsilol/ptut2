
<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Connexion</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <form action="../traitement/connect.php" method="post">
          <input type="hidden" id="page" name="page" value="<?php echo $_SERVER["PHP_SELF"]?>"><br>
          <label for="fname">Adresse mail :</label>
          <input class="form-control" type="email" id="mail" name="mail" placeholder="Exemple : nom@gmail.com"><br>
          <label for="fname">Mot de passe :</label>
          <input class="form-control" type="password" id="password" name="password"><br>
          <input class="btn myBtn" type="submit" value="Connexion"><br><br>
        </form>

          <a href="../pages/inscription.php">Inscription</a><br>
          <a href="../pages/forgotPassword.php">Mot de passe oublié ?</a>
      </div>
    </div>
  </div>
</div>
