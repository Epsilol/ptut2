<?php
    require_once("../model/userFunctions.php");
    require_once("../includes/session.php");
    if(!isUserConnected($_SESSION["mail"],$_SESSION["nom"],$_SESSION["prenom"])){
        header("location: ../index.php");
    }
?>

        <?php
			require_once("../includes/nav.php");
		?>
	<!-- Contenu -->
            <div class="container-fluid">
            <br>
            <h3>Changer mon Mot de Passe</h3>
            <form action="../traitement/changePassword.php" method="post">
            <?php
                echo '<input type="hidden" name="mail" value="'.$_SESSION["mail"].'"/>';
            ?>
            <label>Ancien mot de passe *:</label>
            <input required class="form-control" type="password" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" name="password"><br>
              <label>Nouveau mot de passe *:</label>
              <input required class="form-control" type="password" id="password_" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" name="newPassword"><br>
              <label>Confirmer le nouveau mot de passe *:</label>
              <input required class="form-control" type="password" id="confirm_password_" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" name="confirm_password"><br>
              <input class="btn myBtn" type="submit" value="Valider">
            </form>
            * Le mot de passe doit faire au minimum 8 caractères, et doit contenir au minimum une majuscule, une minuscule, un chiffre et un caractère spécial.

          </div>

<script>
  var password = document.getElementById("password_");
  var confirm_password = document.getElementById("confirm_password_");

  function validatePassword(){
    if(password.value != confirm_password.value) {
      confirm_password.setCustomValidity("Les mots de passes ne correspondent pas.");
    } else {
      confirm_password.setCustomValidity('');
    }
  }

  password.onchange = validatePassword;
  confirm_password.onblur = validatePassword;
</script>
